#include <stdio.h>
#include <stdlib.h>

int getDemand(int p, int r) {
    int temp;
    printf("Enter number of resource %d needed for process %d: ", r, p);
    scanf("%d", &temp);
    return temp;
}

int checkSafety(int P, int R) {
    // Initialization of available resources and max demand matrix
    int *availability = (int *)malloc(sizeof(int) * R);
    int max[P][R];

    for (int i = 0; i < P; i++) {
        for (int j = 0; j < R; j++) {
            max[i][j] = getDemand(i, j);
        }
    }

    // Calculating remaining resources
    int remaining[R];
    for (int j = 0; j < R; j++) {
        remaining[j] = availability[j];
        for (int i = 0; i < P; i++) {
            remaining[j] -= max[i][j];
            if (remaining[j] < 0) {
                free(availability);
                return 0;
            }
        }
    }
    
    free(availability);
    return 1;
}

int main() {
    int P, R;
    printf("Enter number of processes: ");
    scanf("%d", &P);
    printf("Enter number of resources: ");
    scanf("%d", &R);

    if (checkSafety(P, R)) {
        printf("The system is in a safe state.\n");
    } else {
        printf("The system is in an unsafe state.\n");
    }

    return 0;
}
