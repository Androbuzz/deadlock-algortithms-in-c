# Deadlock Algorithms in C

This repository contains implementations of the Banker's algorithm and a resource allocation deadlock detection algorithm in C. These algorithms are part of an assignment for the Operating Systems course at Makerere University. 

The Banker's algorithm is used to ensure safe allocation of resources to processes, while the deadlock detection algorithm identifies potential deadlock situations in resource allocation. The code provides a basic implementation and serves as a learning resource for understanding resource allocation and deadlock avoidance in operating systems.